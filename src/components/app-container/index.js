import styled from 'styled-components';

const AppContainer = styled.div`
    height: 100vh;
`;

export {
    AppContainer
};
