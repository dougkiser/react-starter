import React from 'react';
import {Provider} from 'react-contextual';
import {BrowserRouter as Router} from 'react-router-dom';
import store from './store';
import {Routes} from './routes';
import {AppContainer} from './components';

const App =  () => {
    return (
        <AppContainer>
            <Provider {...store}>
                <Router>
                    <Routes/>
                </Router>
            </Provider>
        </AppContainer>
    );
};

export {App};
