import React from 'react';
import PropTypes from 'prop-types';
import {subscribe} from 'react-contextual';

const Home = props => {
  return (
      <div>
          <span>{props.count}</span>
          <button onClick={props.increment}>Increment</button>
      </div>
  );
};

Home.propTypes = {
    count: PropTypes.number,
    increment: PropTypes.func
};

const HomeSubscriber = subscribe()(Home);

export {Home, HomeSubscriber};
