import React from 'react';
import {shallow, mount} from 'enzyme';
import {Home} from './index';

test('component renders without crashing', () => {
    shallow( <Home/> );
});

test('component displays count', () => {
    const wrapper = mount( <Home count={0}/> );
    expect(wrapper.find('span').text()).toBe('0');
});

test('component renders without crashing', () => {
    const increment = jest.fn();
    const wrapper = mount( <Home count={0} increment={increment}/> );
    wrapper.find('button').simulate('click');
    expect(increment.mock.calls.length).toBe(1);
});
