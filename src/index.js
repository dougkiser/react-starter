import polyfill from 'babel-polyfill'; //eslint-disable-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './app';

ReactDOM.render(<App />, document.getElementById('root'));

if (module.hot) {
    module.hot.accept();
}
