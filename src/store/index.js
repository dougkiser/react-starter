import { buildInformationState, buildActions } from './build-information';

export default ({
  ...buildInformationState,
  ...buildActions,
});
