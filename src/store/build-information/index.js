export const buildInformationState = {
  version: '1.0.0',
  build: '1',
  count: 0
};

export const buildActions = {
    increment: () => state => {
        return ({ count: state.count + 1});
    }
};
