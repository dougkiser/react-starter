import {
    buildInformationState,
    buildActions
} from './index';

test('buildInformationState', () => {
    expect(buildInformationState).toEqual({
        version: '1.0.0',
        build: '1',
        count: 0
    });
});

describe('buildActions', () => {
    test('increment', () => {
        const output = buildActions.increment()({count: 0});
        expect(output.count).toBe(1);
    });
});
